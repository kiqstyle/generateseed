<?php

namespace Kiq;

use Faker\Factory;
use Illuminate\Database\Capsule\Manager as Capsule;
use Illuminate\Support\Pluralizer;

class GenerateSeed
{
    protected $faker;
    protected $tables = [];
    const MYSQLTYPESTOFAKERTYPE = [
        'text' => 'text',
        'varchar' => 'word',
        'integer' => 'numberBetween',
        'int' => 'numberBetween',
        'tinyint' => 'numberBetween',
        'boolean' => 'boolean',
        'date' => 'date',
        'datetime' => 'dateTime'
    ];

    public function __construct()
    {
        $this->faker = Factory::create();
    }

    public function generate()
    {
        $this->getAllTablesInDatabase();
        $this->tables = ['articles'];
        $this->generateAndInsertDumbDataForEachTable();
    }

    private function getAllTablesInDatabase()
    {
        $objectTables = Capsule::select('SHOW TABLES');

        foreach ($objectTables as $index => $objectTable) {
            foreach ($objectTable as $key => $value) {
                // @todo get a list of all ignored tables and dynamically check here
                if ($value === 'phinxlog') {
                    continue;
                } else {
                    $this->tables[] = $value;
                }
            }
        }
    }

    public function generateAndInsertDumbDataForEachTable()
    {
        // path to validation
        $validator = new \Validation;

        foreach ($this->tables as $table) {
            $tableInfoColumns = Capsule::select( Capsule::raw('SHOW COLUMNS FROM ' . $table));

            // buscar path da pasta models
            $modelName = "\\" . Pluralizer::singular($table);
            $modelValidateRulesAttribute = 'validate';

            $insertData = [];
            if (class_exists($modelName)) {
                $model = new $modelName;
                $insertData = [];
                foreach($tableInfoColumns as $column){
                    if ($column->Field === 'id') {
                        continue;
                    }
                    $field = $column->Field;

                    if (array_key_exists($field, $model->$modelValidateRulesAttribute)) {
                        // Aqui pode ser que seja um array de arrays
                        $validationArray = $model->$modelValidateRulesAttribute;
                        $maxValidationsTry = 10;

                        // ex: rules = ['name' => ['minLength' => 3]]; eu preciso somente do minLength
                        $validatorMethod = $validationArray[$field];
                        $fieldToValidate = $this->generateDumbDataByColumnType($column, $validatorMethod);

                        while (!$validator->$validatorMethod($fieldToValidate) && $maxValidationsTry > 0) {
                            $fieldToValidate = $this->generateDumbDataByColumnType($column, $validatorMethod);
                            $maxValidationsTry--;
                        }

                        $insertData[$column->Field] = $fieldToValidate;
                    } else {
                        $insertData[$column->Field] = $this->generateDumbDataByColumnType($column, '');
                    }
                }
            } else {
                foreach($tableInfoColumns as $column){
                    if ($column->Field === 'id') {
                        continue;
                    }

                    $insertData[$column->Field] = $this->generateDumbDataByColumnType($column, null);
                }
            }

            dd($insertData);
            Capsule::table($table)->insertGetId($insertData);
        }
    }

    private function generateDumbDataByColumnType($column, $validatorMethod)
    {
        // se o método for um tipo que o faker reconhece como email, ipv4, slug, url, etc, usar esse método
        $fakerAllowedValidatorMethods = ['email', 'boolean'];
        if (in_array($validatorMethod, $fakerAllowedValidatorMethods)) {
            return $this->faker->$validatorMethod;
        }
        $mysqlTypeToFakerType = self::MYSQLTYPESTOFAKERTYPE;

        // abstrair a classe de validação, buscar o método de validação e tentar criar o tipo
//        $validator = new Validation();
//        var_dump($this->User->validate); die();

        if (!$this->typeHasSize($column)) {
            return $this->faker->$mysqlTypeToFakerType[$column->Type];
        }

        $typeAndSize = explode('(', $column->Type);

        // ignoring unsigned word if type integer
        $typeAndSize[1] = explode(')', $typeAndSize[1])[0];

        if ($this->columnIsForeignKey($column)) {
            return $this->generateDumbDataToForeignKeyColumn($column);
        }

        // @todo Create list of all number types and refactor this code
        if ($typeAndSize[0] === 'int' || $typeAndSize[0] === 'integer' || $typeAndSize[0] === 'tinyint') {
            $maxInteger = 100;
            $minInteger = -100;
            if (strpos($column->Field, 'unsigned')) {
                return $this->faker->$mysqlTypeToFakerType[$typeAndSize[0]](0, $maxInteger);
            }
            return $this->faker->$mysqlTypeToFakerType[$typeAndSize[0]]($minInteger, $maxInteger);
        }
        return $this->faker->$mysqlTypeToFakerType[$typeAndSize[0]];
    }

    /**
     * @param $column
     * @return bool|int
     */
    private function typeHasSize($column)
    {
        return strpos($column->Type, '(');
    }

    /**
     * @param $column
     * @return bool|int
     */
    private function columnIsForeignKey($column)
    {
        return strpos($column->Field, '_id');
    }

    /**
     * @param $column
     * @return bool|string
     */
    private function foreignKeyColumnNameWithoutUnderscoreId($column)
    {
        return substr($column->Field, 0, strpos($column->Field, '_id'));
    }

    /**
     * @param $column
     * @return mixed
     */
    private function generateDumbDataToForeignKeyColumn($column)
    {
        $columnNameWithoutUnderscoreId = $this->foreignKeyColumnNameWithoutUnderscoreId($column);
        $tableNameOfForeignKey = Pluralizer::plural($columnNameWithoutUnderscoreId);
        $objectsWithId = Capsule::select(Capsule::raw('SELECT id FROM ' . $tableNameOfForeignKey));

        $ids = [];

        // convert result objects to array
        foreach ($objectsWithId as $objectWithId) {
            foreach ($objectWithId as $index => $value) {
                $ids[] = $value;
            }
        }

        return $this->faker->randomElement($ids);
    }
}